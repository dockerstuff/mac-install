#!/bin/sh
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install wget
brew install ffmpeg
brew install ansible
brew cask install caffeine
brew cask install gpg-suite
brew cask install sublime-text
brew cask install java
brew cask install google-chrome
brew cask install firefox 
brew cask install vlc 
brew cask install tunnelblick 
brew cask install the-unarchiver
brew cask install telegram
brew cask install macpass
brew cask install thunderbird
brew cask install teamviewer
brew cask install hipchat
brew cask install owncloud 
brew cask install Flycut
brew cask install osxfuse
brew install sshfs
brew install git-crypt
brew install python
brew cask install sourcetree    
brew cask install apache-directory-studio
brew cask install mysqlworkbench
brew cask install sourcetree
brew cask install versions     
brew cask install docker
brew cask install docker-toolbox
brew cask install kitematic
brew cask install wireshark
brew cask install vmware-fusion	
brew cask install sequel-pro	

brew cask install alfred
brew cask install controlplane
brew cask install shimo
brew cask install spotify
brew cask install steam
brew cask install forklift
brew cask install cryptomator
brew cask install phpstorm
brew cask install intellij-idea
brew cask install franz
brew cask install dymo-label
brew cask install skype
brew cask install cord
brew cask install charles
brew cask install versions
brew cask install istat-menus
brew cask install little-snitch
brew cask install amazon-music	
brew cask install bartender


brew cask cleanup
read -p "Allow Apps from everywhere? Does the final Mac user REALLY know, what he is doing? (y/n)? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo spctl --master-disable
fi
